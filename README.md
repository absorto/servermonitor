#  Descentralized Server Uptime Monitor Protocol

### Glossary
+ Target: the server or service being monitored
+ Lead: the server who got the first response from _Target_
+ Checker: remaining servers talking the protocolo with which the Lead will check if the reponse it got is the same as the majority of the Checker's responses
+ Desired Reponse: the response considered OK, might be 200 or any other.

## Protocol Fundamentals

To ensure that the server is monitored correctly, this script must run in at least to servers simultaneously
Decide deterministically (i.e. without talking about it togteher) which server is A and which one is B

Any of the servers running this protocol may issue a request from _Target_, if it got a response, it then takes the _Lead_ role.
_Lead_ asks if _Checkers_ have a response from _Target_, it thens compare the response it got with _Checkers_ response.
If none of the _Checkers_ got a response, _Lead_ response is used but considered non-optimal or trustful. While, if _Checkers_ got their responses
_Lead_ tries to establish a majority.
